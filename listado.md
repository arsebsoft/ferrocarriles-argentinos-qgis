## Listado de ramales

Total de ramales documentados: 441

### Nomenclatura

(S): 375 - Ramales ya representados en el mapa.
(N): 063 - Ramales pendientes de mapeo.
(?): 003 - Ramales que requieren clarificación o más investigación.

### FCDFS
(S): SA: Once a Villa Luro
(S): SB: Villa Luro a Bragado
(S): SC: Bragado a Toay
(S): S1: M. J. Haedo a Emp. J. Mármol
(S): S2: Tablada a Mataderos
(S): S3: Merlo a Empalme Lobos
(S): S4: Luján a Basílica
(S): S5: Suipacha a Bayauca
(S): S6: Gorostiaga a Anderson
(S): S7: Bragado a Bowen
(S): S8: Olascoaga a Timote
(S): S9: Los Toldos a Ing. Luiggi
(S): S10: Lincoln a Telén
(S): S11: Pehuajó a Tres Lomas
(S): S12: Empalme Chivilcoy a Chivilcoy Norte
(S): S13: Tres Lomas a Maza
(S): S14: Maza a Cereales
(S): S15: La Zanja a González Moreno
(S): S16: Valentín Gómez a Winifreda
(S): S17: Darregueira a Huinca Renancó
(S): S18: Darregueira a Remecó
(S): S19: Guatraché a Alpachiri
(S): S20: Rivera a Doblas
(S): S21: Salliqueló a Rivera
(S): S22: La Zanja a Carhué
(S): S23: Carhué a Rivera
(S): S24: Metileo a Arizona
(S): S25: Toay a Emp. Piedra Echada
(S): S26: Villa Luro a Versailles
(S): S27: Villa Luro a Ing.Brian
(S): S28: Estela a Guatraché no construido
(S): S29: Once a Puerto Madero
(S): S30: Colonia Alvear O. a Soitué
(S): S31: Colonia Alvear O. a Carmensa
(S): S32: Macachín a Alpachiri no construido
(S): S34: Remecó a Cotita no construido

FCGR
(S): R1A: Plaza Constitución a Chascomús
(S): R1B: Chascomús a Mar del Plata N.
(S): R1C: Mar del Plata N. a a Puerto M.del Plata
(S): R2: Temperley a Emp km 66
(S): R3: Emp. km 66 a Olavarría
(S): R4: Emp. km 66 a Olavarría
(S): R5: Empalme Lobos a Bolívar a Saavedra
(S): R6: Altamirano a Las Flores
(S): R7: General Guido a Vivoratá
(S): R8: Maipú a Tandil
(S): R9: Las Flores a Tandil
(S): R11: Emp. Lobos a Navarro
(S): R12: Emp. Barrancosa a San Enrique
(S): R13: Emp. Gral.Alvear a Pigüé
(S): R14: Olavarría a Saavedra
(S): R15: Saavedra a Ing. White
(S): R16: Olavarría a Emp. Grünbein
(S): R17: Chas a Ayacucho
(S): R18: Ayacucho a Necochea
(S): R20: Peralta Ramos a Miramar
(S): R21: Quequén a Puerto Quequén
(S): R22: Cte. N. Otamendi a San Agustín
(S): R23: Tamangueyú a Barrow
(S): R24: Tandil Playa a Emp. Lobería
(S): R25: Tandil a Defferrari
(S): R26: Gardey a Ing. White
(S): R27: Defferrari a Emp. Dorrego
(S): R28: Bolívar a Recalde
(S): R29: Recalde a Gral. Lamadrid
(S): R30: Gral. Lamadrid a Pringles
(S): R31: Barrow a Juan E. Barra
(S): R32: Empalme Querandíes a Vela
(S): R33: Azul a Chillar
(S): R34: Chillar a Juan E. Barra
(S): R35: Juan E.Barra a Emp. Dorrego
(S): R36: Temperley a Emp. Temperley
(S): R37: Temperley a Villa Elisa
(S): R38: Kilo 5 a Sola
(S): R39: Kilo 5 a Dock Sud
(S): R40: Ribera sud a Emp.Estévez Norte a Dv.Molinos a Mercado Frutos
(S): R43: Emp. O'Gorman a Emp. Norte
(S): R44: Pereyra a Emp. Destilería YPF
(S): R45: Tolosa a Emp. km 54
(S): R46: Tolosa a R. de Elizalde
(S): R47: La Plata P a Circunvalación
(S): R48: Ringuelet a Cnel. Brandsen
(S): R49: Bosques a Berazategui
(S): R50: Alejandro Korn a San Vicente
(S): R51: Arroyo de los Huesos a Emp. Cerro Chato
(S): R52: Lezama a Don Cipriano
(S): R53: Bajo Hondo a Puerto Belgrano
(S): R54: Emp. Grunbein a Spurr
(S): R55: Emp. Bahía Blanca Sud a Emp. Tablada
(S): R56: Emp. Spurr a Emp. Ing. White Oeste
(S): R57: Emp. Spurr a Emp. Tablada
(S): R58: Ing. White a Garro
(S): R59: Emp. Aguará a Carmen de Patagones
(S): R60: Emp. Ing. White Oeste a Zapala
(S): R61: Cipolletti a km 1218
(S): R62: Bordeu a Emp. vía Neuquén (antes Bordeu a Emp. Aguará)
(S): R64: Emp. Madariaga a Divisadero de Pinamar
(S): R65: Hinojo a Sierras Bayas
(S): R66: Hinojo a Sierra Chica
(S): R67: Emp. Sierras Bayas a Cerro Sotuyo
(S): R68: Emp.Cerro Sotuyo a Cantera Cerro Águila
(S): R70: Olavarría a Loma Negra
(S): R71: Loma Negra a Cantera San Nicolás
(S): R72: Loma Negra a Cantera La Providencia
(S): R73: Emp. Aguirre a Cantera San Luis
(S): R74: Emp. Movediza a Dv. Cantera La Movediza
(?): R75: Dv. Cantera Los Nogales
(S): R76: Dv. Cantera La Movediza a Cantera Cerro Leones
(S): R77: Tandil a Cantera Albión
(S): R81: Emp. Pavón a La Plata P
(S): R84: Monte Veloz a Punta Piedras
(S): R85: La Plata C a Dv. Usina de Gas
(S): R86: R. de Elizalde a Atalaya
(S): R87: Emp. Magdalena a Magdalena
(S): R88: Emp. km 92 a Las Pipinas
(S): R89: R. de Elizalde a Vergara
(S): R90: Bahía Blanca N. O. a Darregueira
(S): R91: Nueva Roma a Villa Iris
(S): R92: ¿Actual S25 Emp. P. Echada a Toay?
(S): R93: Bahía Blanca N. O. a Garro
(S): R94: Emp. Mercado Victoria a Galván
(S): R95: Carmen de Patagones a San Carlos de Bariloche
(S): R96: Emp. Dr. R. Cortizo a Puerto San Antonio Oeste
(S): R97: Gral. L. Vintter a Cnel. F. Sosa
(S): R98: San Lorenzo a Ingenio San Lorenzo
(S): R99: Ing. G. Jacobacci a Esquel
(S): R100: Puerto Madryn a Alto Las Plumas
(S): R101: Emp. R100 (Pto. Madryn) a Aeródromo Pto. Madryn
(S): R103: Trelew a Playa Unión
(S): R104: Las Chapas a Dique Florentino Ameghino
(S): R105: Comodoro Rivadavia a Colonia Sarmiento
(S): R106: Empalme Astra a Astra
(S): R107: Empalme Astra a Comferpet
(S): R108: Puerto Deseado a Las Heras
(S): R109: Ramal a la Base Aeronaval Pto. Deseado
(S): R110: Bahia Blanca RPB / Emp. R26 a Timote
(S): R111: Sierra de la Ventana a Club Hotel Sierra
(?): R112: Gral. Lamadrid a Guaminí NO CONSTRUIDO

FCGSM
(S): SMA: Retiro a Pilar
(S): SMB: Pilar a Junín
(S): SMC: Junín a Villa Mercedes
(S): SMD: Villa Mercedes- Pescadores-San Luis- Mendoza
(S): SME: Mendoza a San Juan
(S): SM-1: Chacarita a Colegiales
(S): SM-2: Caseros a Int. Caseros
(S): SM-3: Caseros a Haedo
(S): SM-4: Rawson a Arribeños
(S): SM-5: Junín a Rojas
(S): SM-6: Chacabuco a Mayor J. Orellano
(S): SM-7: Saforcada a Santa Isabel
(S): SM-8: Alberdi a H. Bouchard
(S): SM-9: Rufino a Emp. Venado Tuerto
(S): SM-10: Rufino a Monte Comán
(S): SM-11: Rufino a La Carlota
(S): SM-12: Laboulaye a Sampacho
(S): SM-13: Laboulaye a Villa Valeria
(S): SM-14: Cañada Verde a Justo Daract
(S): SM-15: Vicuña Mackenna a Sampacho
(S): SM-16: Sampacho a Achiras
(S): SM-17: Km 136 a Villa Mercedes
(S): SM-18: Avanzada a La Paz
(S): SM-19: Villa Mercedes a Villa Dolores
(S): SM-20: Conlara a Los Cerrillos
(S): SM-21: J. N. Lencinas a San Rafael
(S): SM-22: Guadales a Colonia Alvear Norte.
(S): SM-23: Monte Comán a Jaime Prats
(S): SM-24: Monte Comán a San Rafael
(S): SM-25: Pedro Vargas a Bardas Blancas
(S): SM-26: Palmira a Alto Verde
(S): SM-27: Palmira a Tres Porteñas
(S): SM-28: Fray L. Beltrán a Emp. Tropero Sosa
(S): SM-29: Emp. Trasandino a Gral. Gutiérrez
(S): SM-30: Luján de Cuyo a Eugenio Bustos
(S): SM-31: Perdriel a Dest. Luján de Cuyo
(S): SM-32: Monte Comán a Rodolfo Iselín
(S): SM-33: La Hullera a Salagasta
(S): SM-34: Cañada Honda a Albardón
(S): SM-35: San Juan a Desamparados
(S): SM-36: San Juan a Marquesado
(S): SM-37: Espejo a Ing. G. André
(S): SM-38: Villa Krause a Divisoria
(S): SM-39: Km 17 (SM38 Va. Krause a Divisoria) a Emp. SM34 (Caucete)
(S): SM-40: Colonia Alvear N. a Colonia Alvear O.
(S): SM-41: Colonia Alvear O. a Bowen (ant. S7)
(S): SM-43: Fray L. Beltrán a Paraíso
(S): SM-44: General Ortega a Pedregal
(S): SM-45: Saenz Peña a Villa Luro
(S): SM-46: Chacarita a Caballito
(S): SM-48: Crámer a Base Aérea Va. Reynolds

FCGBM
(S): GM1: Retiro a Campana
(S): GM1A: Campana a Zárate
(S): GM1B: Zárate a Rosario Norte
(S): GM1C: Emp. GM1E (Patio Parada) a Sunchales
(S): GM1C20: Variante Emp. GM2 (Cabín 8) a Emp. GM1C (Sarratea)
(S): GM1D: Sunchales a Tucumán
(S): GM1E: Cabín 8 (Ludueña) a Rosario Central
(S): GM1F: Emp. Rosario Central a Túnel Puerto de Rosario
(S): GM1G: Emp. Ludueña a Emp. GM1C (Sorrento)
(S): GM2: Cabín 8 (Ludueña) a Córdoba
(S): GM2A: Túnel al Puerto de Rosario
(S): GM3: Retiro a Tigre
(S): GM3A: Carupá a Muelle San Fernando
(S): GM4: Villa María a Km 136
(S): GM5: Emp. GM1B (San Nicolás) a Pergamino
(S): GM6: Pergamino a Km.7 (Emp. SM5)
(S): GM7: Luján a Vagues
(S): GM8: Vagues a Emp. GM5 (Pergamino)
(S): GM9: Túnel Pto. Rosario a Córdoba
(S): GM10: Casilda a Melincué
(S): GM11: Playa Km 0 (Córdoba) a Malagueño
(S): GM12: Emp. GM1B (San Nicolás) a Puerto de San Nicolás
(S): GM13: Va. Constitución a Río Cuarto
(S): GM14: Cañada de Gómez a Garibaldi
(S): GM15: Pergamino a Cañada de Gómez
(S): GM16: Emp. Coghlan a Delta
(S): GM17: Ferreyra a Dr. A. M. Bass
(S): GM18: Victoria a Vagues
(S): GM19: Cabín 8 (Ludueña) a Santa Teresa a Peyrano
(S): GM19A: Cabín 8 (Ludueña) a Barrio Vila (variante)
(S): GM20: La Banda a Santiago del Estero
(S): GM21: B. de Irigoyen a Santa Fe
(S): GM22: Gálvez a La Rubia
(S): GM23: San Lorenzo a Puerto de San Lorenzo
(S): GM24: Río Segundo a Cantera Alta Gracia
(S): GM25: Cabín 19 (Emp. GM1C) a Embarcaderos
(S): GM26: Cevil Pozo a Km 37 (Requelme)
(S): GM27: Pilar a Villa del Rosario
(S): GM28: Emp. Agua Dulce (GM1D) a Las Colonias
(S): GM29A: Emp. GM1D (Cevil Pozo) a Ingenio Concepción
(S): GM29B: Cevil Pozo a Ingenio San Juan
(S): GM29C: Cevil Pozo a Ingenio Lastenia
(S): GM29D: Emp.GM1D (Cruz del Norte) a Azucarera Concepción
(S): GM29E: San Miguel a Ingenio San Miguel
(S): GM29F: Ranchillos a Ingenio San Vicente
(S): GM30: Ramal a Puerto Borghi
(S): GM31: Emp. GM5 (Pergamino) a Melincué
(S): GM32: Venado Tuerto a Empalme SM5
(S): GM33: Firmat a Chucul
(S): GM34: Km 28 (GM11) a Yocsina
(S): GM35: Barrio Flores a Dumesnil
(S): GM36: Santa Fe a Santa Fe FCGB
(S): GM37: Iturraspe a Villa del Rosario
(S): GM38: Las Trojas a San Ricardo
(S): GM39: Río Cuarto a Río Tercero
(S): GM40: Olmos a J. de la Quintana
(S): GM41: Las Rosas a Villa María
(S): GM42: M. Saavedra a Garibaldi
(S): GM43: M. Saavedra a Emp. GM21 (Matilde)
(S): GM44: Carmen a Guatimozín
(S): GM45: James Craik a Villa del Rosario
(S): GM46: Peyrano a Rastreador Fournier
(N): GM48: Desvío Ingenio Cruz Alta
(?): GM51: Pinto al oeste (D. C. Los Montes)
(S): GM52: Landeta a San Francisco
(S): GM56: Ing. Maschwitz a Dique Luján
(S): GM57: Desvio Cantera Serrano a Cantera Serrano
(S): GM63: San Jorge a Landeta
(S): GM64: Córdoba a Comechingones
(S): GM65: Villa del Rosario a Forres
(S): GM68: RPB a Timote
(S): GM72: Villa María a La Carlota
(N): GM73: Ing. Christiersson a Km 101
(S): GM74: Dr. V. Sarsfield (GM11) a Ika-Renault
(N): GM75: Emp. Firmat (Villada a Cda. del Ucle)
(N): GM76: Emp. Cabín 9 a Cabín 13 (GM9 a GM19)
(S): GM77: Emp. Cabín 9 a ramal GM19 (lado Pérez)

FCGU
(S): UA: Federico Lacroze a Zárate Bajo
(S): UB: Lib. Gral. San Martín a Concordia C
(S): UC: La Criolla a Posadas
(S): UE: Lib. Gral.San Martín a Ibicuy
(S): UF: Dv. Km 88 a Lib. Gral. San Martín
(S): U1: Fátima a 4 de Febrero
(S): U2: E. Carbó a Puerto Ruiz
(S): U3: Emp. Las Colas a Tala
(S): U4: F. M. Parera a Puerto Gualeguaychú
(S): U5: Bajada Grande a C. del Uruguay
(S): U6: Caseros a San Salvador
(S): U7: C. del Uruguay a Concordia C
(S): U8: Villaguay E a Villaguay C
(S): U9: Solá a Km 243
(S): U10: Nogoyá a Victoria
(S): U11: Puerto Diamante a Crespo C
(S): U12: Crespo E a Hasenkamp
(S): U13: San Jaime a La Paz
(S): U14: Federal a Concordia N
(S): U15: Paraná a El Pingo
(S): U16: Hasenkamp a Curuzú Cuatiá
(S): U17: M. F. Mantilla a Puerto de Goya
(S): U18: Corrientes a Mburucuyá
(S): U19: Lomas de Vallejos a Gral. Paz
(N): U20: Ramal al Puerto de Alvear
(N): U21: Ramal al Puerto de Santo Tomé
(S): U22: Rubén Darío a Gral. Lemos
(S): U24: Cnel. F. Lynch a San Martín FCGBM
(S): U25: Concordia N a Concordia C
(N): U26: Ramal a Puerto Ceibo
(S): U27: Salto a Int. Salto FCGB
(S): U28: Ramal al Puerto de Concepción del Uruguay
(N): U30: Ramal al Puerto de Paso de los Libres
(N): U32: Ramal al Puerto 25 de Mayo (ex Puerto San Martín)
(N): U33: Ramal al Puerto de La Cruz
(S): U34: Emp.Km 161 a Corrientes
(N): U35: Ramal al Puerto de Concordia
(S): U37: Ramal al Puerto de La Paz

FCGB
(S): F: Santa Fe SF a Cacuí
(S): F1: Emp. F24 (Santa Fe) a Rosario SF
(S): F2: Emp. F (Santa Fe) a San Cristóbal
(S): F3: Pilar a Villa María salvo est San Francisco
(S): F4: Empalme Santo Tomé a Empalme San Carlos
(S): F5: Gessler a Coronda
(S): F6: Maciel a Puerto Gaboto
(S): F7: Kilómetro 135 a Puerto San Martín
(S): F8: Emp. F1 (Ex Sorrento) a Embarcaderos
(S): F9: Empalme San Carlos a Gálvez
(S): F10: Humboldt a Soledad
(S): F11: Virginia a Moisés Ville
(S): F12: Pozo Del Molle a Carrilobo
(S): F13: Nelson a San Cristóbal
(S): F14: Gobernador Vera a Las Toscas
(S): F15: Intiyaco a Villa Guillermina
(N): F16: Charadai a Santa Sylvina
(N): F17: Horquilla a Km 30
(N): F18: Haumonia a Villa Berthet
(N): F19: General Obligado a Laguna Limpia
(N): F20: Kilómetro 519 a Colonia Baranda
(S): F21: Cacuí a Barranqueras
(N): F22: Villa Ocampo a Ingenio Arno
(N): F23: Cabín 6 a Cabín 9 (Santa Fe)
(S): F24: Emp.F1 a San José del Rincón
(S): RM: Rosario RM a Fuentes
(S): CI: Santa Fe a San Cristóbal
(S): CII: San Cristóbal a Tucumán N.
(S): CIII: Tucumán N. a La Quiaca
(S): C1: Naré a San Javier
(S): C2: Bandera a Añatuya
(S): C3: Añatuya a a Barranqueras
(N): C4: Puna a Santa Justina
(S): C5: Quimilí a Campo Gallo
(S): C6: Tostado a Ap. Gral. Pinedo
(S): C7: Clodomira a Santiago del Estero
(N): C8: Las Cejas a Rosario de la Frontera
(N): C9: Colombres a Toco Palta
(S): C10: Pacará a Las Termas de Río Hondo
(S): C11: Pacará a Ingenio Concepción
(S): C12: Metán a Avia Terai
(S): C13: Güemes a Alemanía
(S): C14: Cerrillos a Socompa
(S): C15: Perico a Pocitos
(S): C16: Pichanal a Orán
(S): C17: El Cadillal a Dique El Cadillal
(S): C18: Joaquín V. González a Pichanal
(N): C19: Rapelli a Pozo Betbeder
(N): C20: Miel de Palo a Los Linares
(S): C21: Emp. Centenario a Tucumán C.
(N): C22: H. Mejía Miraval a Las Tinajas
(S): C23: Tintina a Patay
(S): C24: Roversi a Campo del Cielo
(S): C25: Embarcación a Formosa
(N): C26: Pres. Roque Sáenz Peña a Colonia J. J. Castelli
(N): C27: Pampa de los Guanacos a Sachayoj
(S): C28: Nougués a San Pablo
(N): C29: Pala Pala a Ingenio Bella Vista
(N): C30: Los Juríes a Km 477
(S): C31: Santa Fe a Puerto de Santa Fe
(N): C32: Emp. C3 (Barranqueras) a Puerto Vilelas
(S): C33: Mate de Luna a Tucumán P.
(N): C34: Nougués a Ingenio San Felipe
(N): C35: Lapachito a Zapallar
(N): C36: San Nicolás a Km 20 (ex F. C. San Nicolás a Arroyo Dulce)
(N): C37: Emp. Centenario a Cuarteles
(N): C38: San Andrés a Ingenio San Andrés
(N): C39: Km 789 (Emp. CII) a Ingenio Amalia
(N): C40: Emp. CII (Nougués) a Cía. Azucarera Tucumana
(N): C41: Bustamante a Ingenio San Miguel
(S): CC: Retiro a Tucumán C.
(S): CC1: Emp. Saldías a Puerto Madero
(S): CC2: Triángulo a Puerto de Rosario
(S): CC3: Emp. Graneros a Embarcaderos
(S): CC4: San Francisco a Rafaela SF
(N): CC5: Josefina (Emp. CC4) a Emp. CC4 (Rafaela) / (ant. Rafaela E)
(N): CC6: Cnel. Fraga a Pueblo Marini
(S): CC7: Emp. Garita a Córdoba CA
(N): CC8: Guiñazú a Unquillo
(S): CC9: Caroya a Canteras del Sauce (Km 25)
(N): CC10: Recreo a Chumbicha
(S): CC11: Frías a Santiago del Estero CC
(S): CC12: Emp. CC (Tucumán) a Villa Alberdi
(S): CC13: Río Colorado a Famaillá
(S): CC14: Villa Alberdi a La Cocha
(N): CC15: Aguilares a Los Sarmientos
(N): CC16: Concepción a Medinas
(N): CC17: Arcadia a Gastona
(S): CC18: 24 de Septiembre a Km 15 (Muñecas)
(S): CC19: Don Torcuato a Km 37
(S): CC20: Emp. CC (Tucunán) a Tucumán P.
(N): CC21: Dv. Km 18 a Canteras de Ancaján (Km 15)
(N): CC22: Río Chico a Canteras de la Nueva Trinidad
(N): CC23: Villa Allende a Canteras San Fernando
(S): CC24: Emp. Frontera a Sa Emp. Enlace San Francisco SF a CC
(N): CC27: Recreo a Cerro Rico
(S): G: Buenos Aires a Rosario CGBA
(S): G1: Buenos Aires a Riachuelo
(S): G2: Villa Madero a Mataderos
(S): G3: González Catán a Puerto La Plata
(S): G4: Villars a Gral. Villegas
(S): G5: Patricios a Victorino de la Plaza
(S): G6: Pergamino a Vedia
(N): G7: Puerto Rosario a Emp. F1 (Rosario SF)
(N): G10: Emp. G7 (Puerto Rosario)
(S): P: Emp. G3 (Puerto La Plata) a Mira Pampa
(S): P1: Emp. Gambier a Avellaneda
(S): P2: Carlos Beguerie a Dv. Loma Negra
(S): P3: Ariel a Tte. Cnel. Miñana
(S): P4: Tte. Cnel. Miñana a Sierra Chica
(S): P5: Pedro Gamen a Pehuajó
(S): M: Puente Alsina a Carhué
(S): M1: Puente Alsina a Intercambio Midland
(S): A: Laguna Paiva a Catamarca
(S): A1: Emp.Garita a Cosquín a Cruz del Eje
(S): A2: Serrezuela a San Juan
(S): A3: Patquía a Chilecito
(S): A4: Cebollar a Andalgalá
(S): A5: Mazán a Tinogasta
(S): A6: Catamarca a La Cocha
(S): A7: Coll a Jáchal
(N): A8: La Puerta a Córdoba CNA
(N): A9: Milagro a Quines
(S): A10: Pie de Palo a Mendoza Estado
(N): A11: Santa Rosa del Río Primero a Tránsito
(S): A12: Emp. Buena Nueva (A10) a Las Cuevas
(N): A13: Cte. Leal a Pinas
(N): A14: Thea a Canteras
(N): A15: José Martí a Las Casuarinas
(S): A16: Mendoza P a Paso de los Andes
(N): A17: Caucete a Villa Independencia
(N): A19: Emp. A2 (Angaco Sud) a Dv. Bodega Tinto Hnos.
(N): A20: Emp. A2 (Angaco Sud) a Dv. Bodega Meglioli a San Isidro
(N): A21: Emp. A2 (Pedro Echagüe) a Dv. Bodega Gutiérrez
(N): A22: Emp. A2 (Las Chimbas) a Dv. Bodega Graffigna
(N): A23: Emp. A7 (Angaco Norte) a Dv. Bodega Muro y Bustello
(N): A24: Cablecarril Chilecito a La Mejicana
