# sig-ferrocarrilesargentinos
SIG Ferrocarriles Argentinos

Este repositorio contendrá los archivos Spatialite necesarios para armar el SIG de nuestra red ferroviaria nacional.

Este SIG propone una sustancial mejora al que actualmente puede encontrarse en
http://goo.gl/LAqybs.

Propongo corregir y mejorar la información geográfica actual, añadiendo datos
necesarios tales como codificaciones de ramales, enrieladura, electrificación,
señalización, estado del ramal, etc. Los datos esenciales se brindan en las
tablas, y la información adicional se obtendrá mediante consultas SQL.

## Documentación del proyecto

* [Issues](https://bitbucket.org/arsebsoft/sigferrocarrilesargentinos/issues?status=new&status=open)
* [Hitos](https://bitbucket.org/arsebsoft/sigferrocarrilesargentinos/admin/issues/milestones)
* [Wiki](https://bitbucket.org/arsebsoft/sigferrocarrilesargentinos/wiki/)
